from typing import List, Optional
import numpy as np
from rdkit import Chem
from rdkit.Chem import AllChem
from ase import Atoms

from .utils import next_neighbor
from .translate import atoms_to_rdkit, rdkit_to_atoms
from .bondlength import bondlength


def repeat_monomer(
        monomer: Atoms, connections: List[int], n: int,
        r: Optional[float] = None, optimize: bool = False) -> Atoms:
    h1, h2 = connections

    polymer = monomer
    for i in range(n - 1):
        polymer = add(monomer, h2, polymer, h1, r, optimize)

    return polymer


def add(atoms1: Atoms, h1: int, atoms2: Atoms, h2: int,
        r: Optional[float] = None, optimize: bool = False) -> Atoms:
    """Add two structures by removing Hs and connect there
    the resulting structure can be optimized to avoid overlap
    """
    c1 = next_neighbor(atoms1, h1)
    dir1_c = atoms1[h1].position - atoms1[c1].position
    dir1_c /= np.linalg.norm(dir1_c)

    atoms1 = atoms1.copy()

    c2 = next_neighbor(atoms2, h2)
    dir2_c = atoms2[h2].position - atoms2[c2].position
    dir2_c /= np.linalg.norm(dir2_c)

    if r is None:
        r = bondlength(atoms1[c1].symbol, atoms2[c2].symbol)
    assert r is not None, "Unknown bondlength for \
                {atoms1[c1].symbol} and {atoms1[c2].symbol}. Please give r."

    atoms2 = atoms2.copy()
    atoms2.translate(- atoms2[c2].position)
    atoms2.rotate(dir2_c, -dir1_c)
    atoms2.translate(atoms1[c1].position + dir1_c * r)

    if not optimize:
        del atoms1[h1]
        del atoms2[h2]
        return atoms1 + atoms2

    rd1 = atoms_to_rdkit(atoms1)
    rd2 = atoms_to_rdkit(atoms2)

    rd1 = Chem.EditableMol(rd1)
    rd1.RemoveAtom(h1)
    rd1 = rd1.GetMol()
    n1 = rd1.GetNumAtoms()

    rd2 = Chem.EditableMol(rd2)
    rd2.RemoveAtom(h2)
    rd2 = rd2.GetMol()

    rdadd = Chem.CombineMols(rd1, rd2)
    rdadd = Chem.EditableMol(rdadd)

    # bond indices have to be modified if the removed hydrogen
    # changes them
    b1 = c1
    if h1 < c1:
        b1 -= 1
    b2 = c2
    if h2 < c2:
        b2 -= 1
    rdadd.AddBond(b1, n1 + b2, Chem.BondType.SINGLE)
    rdadd = rdadd.GetMol()
    Chem.GetSSSR(rdadd)
    AllChem.MMFFOptimizeMolecule(rdadd)

    return rdkit_to_atoms(rdadd)
