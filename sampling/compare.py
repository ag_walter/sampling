import numpy as np
from typing import List


def r2(observed: List[float], predicted: List[float]) -> float:
    """Return the coefficient of determination
    for the comparison of two arrays
    https://en.wikipedia.org/wiki/Coefficient_of_determination
    """
    obs = np.array(observed)
    pred = np.array(predicted)
    assert obs.shape == pred.shape

    def sqsum(array):
        return (array**2).sum()

    return 1 - sqsum(obs - pred) / sqsum(obs - obs.mean())
