import tempfile
import numpy as np

try:
    from openbabel import pybel
except ModuleNotFoundError:
    pybel = None
from rdkit import Chem
from rdkit.Chem import AllChem
from rdkit.Chem import rdDetermineBonds

from ase import Atoms, Atom


def rdkit_to_atoms(mol, positions=None):
    """Create Atoms from rdkit mol

    mol: rdkit mol object
    positions: optionally give positions
    """
    na = len(mol.GetAtoms())

    if positions is None:
        if not len(mol.GetConformers()):
            AllChem.EmbedMultipleConfs(mol, numConfs=1)
        conf = mol.GetConformer()
        positions = np.empty((na, 3))
        for ia in range(na):
            pos = conf.GetAtomPosition(ia)
            positions[ia] = [pos.x, pos.y, pos.z]
    else:
        positions = np.array(positions)
        assert positions.shape == (na, 3)

    atoms = Atoms()
    for rdatom, position in zip(mol.GetAtoms(), positions):
        atoms.append(Atom(rdatom.GetSymbol(), position))
    return atoms


def atoms_to_openbabel(atoms: Atoms):
    # hack to get bonds
    with tempfile.NamedTemporaryFile(suffix='.xyz') as tf:
        atoms.write(tf.name)
        mol = next(pybel.readfile('xyz', tf.name))

    return mol


def openbabel_to_rdkit(mol):
    return Chem.MolFromMolBlock(mol.write('mol'), removeHs=False)


def atoms_to_rdkit(atoms: Atoms, bind: bool = True):
    with tempfile.NamedTemporaryFile(suffix='.xyz') as tf:
        atoms.write(tf.name, format='xyz')
        raw_mol = Chem.MolFromXYZFile(tf.name)
    mol = Chem.Mol(raw_mol)
    if bind:
        rdDetermineBonds.DetermineConnectivity(mol)
        try:
            rdDetermineBonds.DetermineBondOrders(mol, charge=0)
        except ValueError:  # DetermineBondOrders sometimes fails
            pass
    Chem.SanitizeMol(mol)
    return mol


def to_smiles(atoms: Atoms, removeHs: bool = True) -> str:
    """Translate an ase.Atoms object to a smiles string"""
    mol = atoms_to_rdkit(atoms)
    if removeHs:
        mol = Chem.RemoveHs(mol)
    return Chem.MolToSmiles(mol)


def from_smiles(smiles: str) -> Atoms:
    """Create ase.Atoms from a smiles string"""
    mol = Chem.MolFromSmiles(smiles)
    mol = Chem.AddHs(mol)
    return rdkit_to_atoms(mol)


class AtomsCreator:
    def smiles(self, smile_string):
        """Create atoms from a smiles string"""
        return from_smiles(smile_string)

    def rdkit(self, mol, positions=None):
        """Create atoms from a rdkit mol"""
        return rdkit_to_atoms(mol, positions)
