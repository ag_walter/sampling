import numpy as np
from ase import Atoms


def walk(N: int, length: float = 1, element: str = "H", rng=np.random)\
        -> Atoms:
    atoms = Atoms(element * N)

    pos_c = np.zeros(3)
    for i in range(N):
        atoms[i].position = pos_c
        pos_c[rng.choice(range(3))] += rng.choice([1, -1]) * length

    return atoms


def self_avoiding_walk(
    N: int, length: float = 1, element: str = "H", rng=np.random
) -> Atoms:
    atoms = Atoms(element * N)

    z_ac = []
    z_c = [0, 0, 0]
    for i in range(N):
        atoms[i].position = length * np.array(z_c)
        z_ac.append(z_c.copy())
        zold_c = z_c.copy()

        iter = 0
        while z_c in z_ac:
            z_c = zold_c.copy()
            z_c[rng.choice(range(3))] += rng.choice([1, -1])
            iter += 1

    return atoms
