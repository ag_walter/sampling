import numpy as np

from ase.calculators.calculator import Calculator, all_changes
import ase.units as u

from rdkit.Chem.rdForceFieldHelpers import UFFGetMoleculeForceField
from rdkit.Geometry import Point3D

from sampling.translate import atoms_to_rdkit


class UFF(Calculator):
    implemented_properties = ['energy', 'forces']

    def calculate(
            self, atoms=None, properties=None,
            system_changes=all_changes):

        if properties is None:
            properties = self.implemented_properties

        Calculator.calculate(self, atoms, properties, system_changes)

        if atoms is None:
            atoms = self.atoms

        if 'numbers' in system_changes:
            self.mol = atoms_to_rdkit(atoms)
        elif 'positions' in system_changes:
            conf = self.mol.GetConformer()
            for i in range(self.mol.GetNumAtoms()):
                conf.SetAtomPosition(i, Point3D(*atoms[i].position))

        ff = UFFGetMoleculeForceField(self.mol)
        self.results['energy'] = ff.CalcEnergy() * u.kcal / u.mol
        forces = np.array(ff.CalcGrad()).reshape(len(atoms), 3)
        self.results['forces'] = - forces * u.kcal / u.mol
