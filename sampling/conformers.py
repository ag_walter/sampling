import numpy as np
from rdkit.Chem import AllChem
from typing import Optional
from copy import copy

from ase import io, Atoms
from ase.io.trajectory import TrajectoryWriter
from ase.optimize import BFGS
from ase.geometry import distance

from .translate import AtomsCreator, atoms_to_rdkit
from .optimize.internal import internal_or_fire
from .geometry import align
from .typing import MaybeDefinedInitiliaze, DefinedInitiliaze


class ConformerBase(list):
    """Conformer generation base class"""
    def sort(self):
        """Sort conformers by energy"""
        super().sort(key=lambda atoms: atoms.get_potential_energy())

    def define_initialize(
            self, initialize: MaybeDefinedInitiliaze
            ) -> DefinedInitiliaze:
        if initialize is not None:
            return initialize

        def defined_initialize(atoms):
            atoms.calc = copy(self.calc)
            return atoms

        return defined_initialize

    def relax(self,
              initialize: MaybeDefinedInitiliaze = None,
              fmax: float = 0.05,
              optimizer=internal_or_fire,
              trajname: Optional[str] = None) -> None:
        """Relax all conformers

        fmax: maximal force (ignored)
        optimizer: Optimizer to use, default Internal
        initialize: function to initialize the atoms object,
          must return the modified atoms object
        trajname: name of the trajectory to save relaxed structures
        """
        defined_initialize = self.define_initialize(initialize)

        for i, atoms in enumerate(self):
            super().__setitem__(
                i, self._relax_and_save(
                    atoms, defined_initialize, fmax, optimizer, trajname))

    @staticmethod
    def _relax(atoms: Atoms,
               initialize: DefinedInitiliaze,
               fmax: float = 0.05,
               optimizer=internal_or_fire) -> Atoms:
        atoms = initialize(atoms)
        opt = optimizer(atoms)
        opt.run(fmax=fmax)
        return atoms

    def _relax_and_save(self, atoms: Atoms,
                        initialize: DefinedInitiliaze,
                        fmax: float = 0.05,
                        optimizer=internal_or_fire,
                        trajname: Optional[str] = None) -> Atoms:
        atoms = self._relax(
            atoms, initialize, fmax, optimizer)
        if trajname:
            with io.Trajectory(trajname, 'a') as traj:
                traj.write(atoms)
        return atoms

    def write(self, trajname: str):
        with io.Trajectory(trajname, 'w') as traj:
            self._write_traj(traj)

    def _write_traj(self, traj: TrajectoryWriter):
        for atoms in self:
            traj.write(atoms)

    @classmethod
    def read(cls, filename: str):
        """Read conformers from a file"""
        with io.Trajectory(filename) as traj:
            confs = Conformers(traj[0])
            for image in traj[1:]:
                assert len(image) == len(traj[0])
                confs.append(image)
            return confs
        assert 0, f'{filename} is not a ase.io.Trajectory file.'

    def remove_similar(self, dmin: float) -> int:
        """Remove all conformers within a distance dmin"""
        self.align()

        j = 1
        while j < len(self):
            for i in range(j):
                d = distance(self[i], self[j])
                if d < dmin:
                    self.pop(j)
                    j -= 1
                    break
            j += 1

        return len(self)

    def align(self) -> None:
        for conf in self:
            align(conf)


class Conformers(ConformerBase):
    def __init__(self, atoms):
        self.calc = atoms.calc
        self._mol = atoms_to_rdkit(atoms.copy())
        super().append(atoms.copy())

    def add(self, number: int, parameters={}):
        """Add a given number of conformers"""
        # see https://www.rdkit.org/docs/Cookbook.html\
        #    #conformer-generation-with-etkdg
        settings = {
            'useSmallRingTorsions': True,
            'pruneRmsThresh': 0.5,
            'useRandomCoords': True,
            }
        settings.update(parameters)

        params = AllChem.ETKDG()
        for key, value in settings.items():
            setattr(params, key, value)

        missing = number
        while missing > 0:
            cids = AllChem.EmbedMultipleConfs(
                self._mol, numConfs=missing, params=params)

            for i, conf in enumerate(self._mol.GetConformers()):
                atoms = AtomsCreator().rdkit(self._mol,
                                             conf.GetPositions())
                super().append(atoms)

            missing -= len(cids)

    def initialize(self, atoms):
        atoms.calc = self.calc
        return atoms


class RandomBoxConformers(ConformerBase):
    def __init__(self, atoms):
        self.calc = atoms.calc
        self.atoms = atoms.copy()
        super().append(atoms.copy())

    def add(self, number, fmax=0.5):
        """Works, but is increadibly slow"""
        cell = np.array([10, 10, 10])  # XXX change cell
        atoms = self.atoms.copy()
        atoms.calc = self.calc
        atoms.cell = cell

        for i in range(number):
            atoms.set_scaled_positions(
                np.random.random((len(self.atoms), 3)))

            opt = BFGS(atoms, trajectory=f'relax{i}.traj')
            opt.run(fmax=fmax)

            super().append(atoms.copy())
