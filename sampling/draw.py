from ase import Atoms
from rdkit import Chem
from rdkit.Chem import Draw

from .translate import to_smiles


def draw(atoms: Atoms):
    """Draw atoms using rdkit"""
    molst = [Chem.MolFromSmiles(to_smiles(atoms))]
    return Draw.MolsToGridImage(molst)
