from typing import Callable, Optional
import numpy.typing as npt
from ase import Atoms


DefinedInitiliaze = Callable[[Atoms], Atoms]
MaybeDefinedInitiliaze = Optional[Callable[[Atoms], Atoms]]

Vector = npt.NDArray
