from typing import List, Optional
import numpy as np

from ase import Atoms
from ase.data import covalent_radii
from ase.neighborlist import NeighborList

from .typing import Vector
from .bondlength import bondlengths

nbonds = {
    'C': 4,
    'N': 3,
    'O': 2,
    'S': 2,
}


def bond_order(atoms: Atoms, a0: int, a1: int) -> float:
    """bond order from bondlength

    assume linear dependence, see SI of DOI: 10.1039/C5CP00435G"""
    # count H as one
    if atoms[a0].symbol == 'H' or atoms[a1].symbol == 'H':
        return 1

    bondlength = atoms.get_distance(a0, a1)
    symbols = ''.join(sorted([atoms[a0].symbol, atoms[a1].symbol]))
    lengths = bondlengths[symbols]
    m = 1 / (lengths[1] - lengths[0])
    a = 3 / 2 - m * (lengths[1] + lengths[0]) / 2
    bo = m * bondlength + a
    return bo


def sp(atoms: Atoms, ia: int) -> Optional[int]:
    """Determine hybridization"""
    symbol = atoms[ia].symbol
    if symbol not in nbonds:
        return None

    sp = 0
    for ib in bonded_indices(atoms, ia):
        bo = bond_order(atoms, ia, ib)
        if not sp and int(bo + 0.5) == 3:
            sp = 1   # triple bond -> sp1
        if not sp and int(bo + 0.5) == 2:
            sp = 2
    if not sp:
        sp = 3
    assert int(bo + 0.5) <= nbonds[symbol]

    return sp


def sp2_vector(atoms: Atoms, ia: int):
    ibs = bonded_indices(atoms, ia)

    pos_ac = atoms.get_positions()
    pos_ac -= pos_ac[ia]
    if len(ibs) == 3:
        sum_c = - (np.cross(pos_ac[ibs[0]], pos_ac[ibs[1]])
                   + np.cross(pos_ac[ibs[1]], pos_ac[ibs[2]])
                   + np.cross(pos_ac[ibs[2]], pos_ac[ibs[0]]))
    elif len(ibs) == 2:
        sum_c = np.cross(pos_ac[ibs[0]], pos_ac[ibs[1]])
    else:
        raise RuntimeError('need 2 or 3 neighbors')
    return sum_c / np.linalg.norm(sum_c)


def bonded_indices(atoms: Atoms, index: int, scale: float = 1.5) -> List[int]:
    """Return list of indices of bonded atoms"""
    radii = scale * covalent_radii[atoms.get_atomic_numbers()]
    nl = NeighborList(radii, skin=0, self_interaction=False, bothways=True)
    nl.update(atoms)
    return nl.get_neighbors(index)[0]


def lone_pair_direction(
        atoms: Atoms, index: int,
        bonded: Optional[List[int]] = None) -> Vector:
    # return the direction where to find the lone pairs
    if bonded is None:
        bonded = bonded_indices(atoms, index)

    assert len(bonded) == 2 or len(bonded) == 3, f'bonded: {bonded}'

    pos_ac = atoms.get_positions() - atoms[index].position
    if len(bonded) == 3 and sp(atoms, index) == 2:
        vec = np.cross(pos_ac[bonded[0]], pos_ac[bonded[1]])
    else:
        vec = np.zeros(3)
        for ia in bonded:
            dvec = pos_ac[ia]
            vec -= dvec / np.linalg.norm(dvec)
    return vec / np.linalg.norm(vec)
