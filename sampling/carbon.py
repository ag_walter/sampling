import numpy as np
import numpy.typing as npt
from typing import Dict, List

from ase import Atoms
from ase.neighborlist import NeighborList


def coordination(atoms: Atoms, rcut: float = 1.85) -> List[Dict[str, int]]:
    # define coordination through cutoff
    nl = NeighborList([rcut / 2.] * len(atoms), skin=0,
                      bothways=True, self_interaction=False)
    nl.update(atoms)

    coord = []
    for ia, a in enumerate(atoms):
        indices, offsets = nl.get_neighbors(ia)
        elements: Dict[str, int] = {}
        for j, offset in zip(indices, offsets):
            sym = atoms[j].symbol
            if sym in elements:
                elements[sym] += 1
            else:
                elements[sym] = 1
        coord.append(elements)
    return coord


def count(atoms: Atoms, symbol: str = 'C', cmax: float = 9) -> npt.NDArray:
    """Percentage of atoms with given symbol and coordination."""
    coord = coordination(atoms)
    counts = np.zeros(int(cmax + 1.5))
    for coo, atom in zip(coord, atoms):
        if atom.symbol == symbol:
            counts[np.sum(list(coo.values()))] += 1
    counts *= 100 / counts.sum()
    # check for over-coordinated C's
    assert counts[7:].sum() == 0
    return counts
