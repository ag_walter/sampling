import numpy as np
import pytest

from ase.build import molecule

from sampling.utils import next_neighbor, random_perpendicular_vector


def test_next_neighbor():
    atoms = molecule('HCOOCH3')
    assert 4 == next_neighbor(atoms, 6)
    assert 4 == next_neighbor(atoms, -1)
    assert 0 == next_neighbor(atoms, 1)


def test_random_perpendicular_vector():
    d_c = [2, 1, 3]
    rng = np.random.RandomState(42)
    hrp_c = random_perpendicular_vector(d_c, rng=rng)

    assert np.linalg.norm(hrp_c) == pytest.approx(1)  # unit vector
    assert np.dot(d_c, hrp_c) == pytest.approx(0)  # prependicular
