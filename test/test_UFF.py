import pytest
openbabel = pytest.importorskip("openbabel")

from ase.build import molecule
import ase.units as u

from sampling.calculators.uff import UFF
from sampling.calculators.pybel import Pybel


def test_pybel():
    """Test agreement to openbabel implementation"""
    atoms = molecule('H2O')
    atoms.calc = Pybel(forcefield='uff')
    # XXX something wrong with energies/force units in pybel?
    Epybel = atoms.get_potential_energy() * u.kJ / u.kcal
    Fpybel = atoms.get_forces() * u.kJ / u.kcal

    atoms.calc = UFF()

    assert atoms.get_potential_energy() == pytest.approx(Epybel, 1e-2)
    assert atoms.get_forces() == pytest.approx(Fpybel, abs=1e-2)
