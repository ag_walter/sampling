import numpy as np
from ase.build import molecule

from sampling.vdw_conformers import VdwConformers
from sampling.calculators.uff import UFF


def test_build():
    atoms1 = molecule('CO')
    atoms2 = molecule('H2O')

    confs = VdwConformers(atoms1, atoms2)
    rng = np.random.RandomState(42)
    confs.add(1, distance=2, rng=rng)
    confs.add(1, distance=2, rng=rng)

    assert len(confs) == 2
    for conf in confs:
        assert len(conf) == len(atoms1) + len(atoms2)

    fname = 'confs.traj'
    confs.write(fname)

    confs2 = VdwConformers.read(fname)
    assert confs2.atoms1 == confs.atoms1
    assert confs2.atoms2 == confs.atoms2
    assert len(confs2) == len(confs)


def test_relax():
    atoms1 = molecule('CO')
    atoms2 = molecule('H2O')

    confs = VdwConformers(atoms1, atoms2)
    rng = np.random.RandomState(42)
    confs.add(1, distance=2, rng=rng)

    def initialize(atoms):
        atoms.calc = UFF()
        return atoms

    fmax = 0.01
    trajname = 'relaxed.traj'
    confs.relax(fmax=fmax, initialize=initialize,
                trajname=trajname)
    # test that relaxed are saved
    confs2 = VdwConformers.read(trajname)
    assert len(confs2) == len(confs)

    for atoms in [confs.atoms1, confs.atoms2] + confs:
        assert np.abs(atoms.get_forces()).max() <= fmax
