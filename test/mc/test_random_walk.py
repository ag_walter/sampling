import pytest
import numpy as np
from sampling.mc.random_walk import walk, self_avoiding_walk


def test_random_walk():
    N = 3
    length = 0.7
    rng = np.random.RandomState(42)  # ensure the same seed
    polymer = walk(N, length=length, rng=rng)

    assert len(polymer) == N
    for i in range(N - 1):
        assert polymer.get_distance(i, i + 1) == pytest.approx(length)


def test_self_avoiding_random_walk():
    N = 10
    length = 0.7
    rng = np.random.RandomState(42)  # ensure the same seed
    polymer = self_avoiding_walk(N, length=length, rng=rng)

    assert len(polymer) == N
    for i in range(N - 1):
        assert polymer.get_distance(i, i + 1) == pytest.approx(length)
        for j in range(i + 1, N):
            assert polymer.get_distance(i, j) >= 0.9 * length

    # polymer.edit()
