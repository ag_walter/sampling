from sampling.translate import from_smiles
from sampling.calculators.uff import UFF
from sampling.conformers import RandomBoxConformers


def test_build():
    atoms = from_smiles('CCC')
    atoms.calc = UFF()
    atoms.get_potential_energy()

    confs = RandomBoxConformers(atoms)
    confs.add(1)
