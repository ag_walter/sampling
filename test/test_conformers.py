import pytest
import numpy as np
from ase import io

from sampling.translate import from_smiles
from sampling.conformers import Conformers
from sampling.calculators.mmff94 import MMFF94
from sampling.calculators.uff import UFF


def test_build():
    atoms = from_smiles('CC(C)Cc1ccc(cc1)C(C)C(=O)O')

    confs = Conformers(atoms)
    confs.add(1)
    assert len(confs) == 2  # includes initial structure
    confs.add(1)
    assert len(confs) == 3
    confs.write('conf_initial.traj')

    try:
        confs.sort()
    except RuntimeError:
        pass
    else:
        raise 'Sorting should fail as we did not assign calc yet'

    # energy before relaxation
    ene0 = []
    for conf in confs:
        conf.calc = MMFF94()
        ene0.append(conf.get_potential_energy())
    ene0 = np.array(ene0)

    def initialize(atoms):
        atoms.calc = MMFF94()
        return atoms

    confs.relax(initialize=initialize)
    ene1 = np.array([conf.get_potential_energy()
                     for conf in confs])
    assert (ene1 < ene0).all()
    confs.write('conf_relaxed.traj')

    confs.sort()
    ene2 = np.array([conf.get_potential_energy()
                     for conf in confs])
    assert (ene2[1:] - ene2[:1] > 0).all()
    confs.write('conf_sorted.traj')


def test_relax_initialize():
    """Test relaxation with arbitrary calculator"""
    atoms = from_smiles('CC')
    atoms2 = atoms.copy()

    atoms.calc = MMFF94()
    E0 = atoms.get_potential_energy()

    confs = Conformers(atoms)
    confs.add(1)

    trajname = 'relaxed.traj'
    confs.relax(trajname=trajname)
    # check that everything is there
    assert len(io.Trajectory(trajname))

    confs.sort()

    # make sure that the initial structure is untouched
    atoms2.calc = MMFF94()
    assert atoms2.get_potential_energy() == pytest.approx(E0)


def test_reduce(testdata):
    """Reduce to dissimilar conformaers"""
    confs = Conformers.read(testdata / 'CCCC_confs.traj')

    confs.remove_similar(2)
    assert len(confs) == 3

    # second call should not do anything
    confs.remove_similar(2)
    assert len(confs) == 3


def test_align(testdata):
    """Align conformers for better comaprability"""
    confs = Conformers.read(testdata / 'CCCC_confs.traj')

    confs.align()
    for conf in confs:
        assert conf.get_center_of_mass() == pytest.approx(np.zeros(3))


def test_energy_in_trajectory():
    atoms = from_smiles('CCCC')
    atoms.calc = UFF()

    trajname = 'confs.traj'
    confs = Conformers(atoms)
    confs.add(1)
    confs.relax()
    confs.sort()
    confs.write(trajname)

    confs = io.Trajectory(trajname)

    # ensure the energy is there and the structures are sorted
    Emax = -1e23
    for atoms in confs:
        assert Emax < atoms.get_potential_energy()
        Emax = atoms.get_potential_energy()
