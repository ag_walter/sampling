import pytest
from ase import Atoms
from ase.build import molecule
from ase.calculators.emt import EMT
from ase.optimize import FIRE
from ase.vibrations import Vibrations

from sampling.thermochemistry import (
    geometry, thermochemistry)


def test_geometry():
    assert geometry(Atoms('H')) == 'monatomic'

    # XXX do we have a more sensible value for a tiny deviation
    # of linearity?
    for delta, geo in zip([1e-5, 0.2], ['linear', 'nonlinear']):
        atoms = Atoms('H3',
                      positions=[[0, 0, -1], [0, delta, 0], [0, 0, 1]])
        assert geometry(atoms) == geo


def test_H2O():
    atoms = molecule('H2O')
    atoms.calc = EMT()
    FIRE(atoms).run(fmax=0.05)
    name = 'vib'
    vibrations = Vibrations(atoms, name=name)
    vibrations.run()

    # previous calculation
    expected = {
        None: 1.8791735093002786,
        'zpe': 2.236755604630236,
        'enthalpy': 2.3533654566572837,
        'gibbs': 1.741931927811761,
    }

    for correction in expected.keys():
        assert expected[correction] == pytest.approx(
            thermochemistry(atoms, vibrations, correction))
