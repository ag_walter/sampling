import pytest
from ase.build import molecule
from ase import io

from sampling.translate import to_smiles, from_smiles
from sampling.translate import atoms_to_rdkit, rdkit_to_atoms


def test_smiles():
    smiles = 'CCC'
    atoms = from_smiles(smiles)

    # different smiles, but same atoms
    smiles1 = to_smiles(atoms, removeHs=False)
    assert (atoms.symbols
            == from_smiles(smiles1).symbols).all()

    # same smiles
    smiles2 = to_smiles(atoms)
    assert smiles2 == smiles


def test_benzene():
    """Check that bonds are done correctly resulting in standard smiles"""
    atoms = molecule('C6H6')
    assert to_smiles(atoms) == 'c1ccccc1'


def test_O2_smiles():
    """O2 smiles reconstruction fails"""
    atoms = molecule('O2')
    assert to_smiles(atoms) == 'O=O'


@pytest.mark.skip(reason='randomly (?) failes to create 3D isomer')
def test_difficult_smiles():
    smiles = 'CCCCCCCCCCOC(=O)c1cccc(C(=O)OCCCCCCCCCC)c1C(=O)OCCCCCCCCCC'
    atoms = from_smiles(smiles)

    assert atoms.get_chemical_formula() == 'C39H66O6'


def test_indices_and_positions():
    """Check that translating keeps indices and positions"""
    atoms = from_smiles('O')
    mol = atoms_to_rdkit(atoms)
    btoms = rdkit_to_atoms(mol)

    i = 0
    for atom, rdtom, btom in zip(atoms, mol.GetAtoms(), btoms):
        assert atom.symbol == btom.symbol
        assert atom.symbol == rdtom.GetSymbol()
        assert atom.position == pytest.approx(btom.position)
        assert rdtom.GetIdx() == i
        rdpos = mol.GetConformer().GetAtomPosition(i)
        assert atom.position[0] == pytest.approx(rdpos.x)
        assert atom.position[1] == pytest.approx(rdpos.y)
        assert atom.position[2] == pytest.approx(rdpos.z)
        i += 1


# output of to_smiles for TCNQ
smiles_tcaq = 'N#CC(C#N)=c1ccc(=C(C#N)C#N)cc1'


def test_tcnq_selfconsitent():
    """Check that own smiles works"""
    atoms = from_smiles(smiles_tcaq)
    smiles = to_smiles(atoms)
    assert smiles == smiles_tcaq


def test_tcnq_dft(testdata):
    atoms = io.read(testdata / 'tcnq_dft.xyz')
    assert to_smiles(atoms) == smiles_tcaq


@pytest.mark.skip('This test sometimes fails')
def test_cis_trans_methyls():
    # see https://github.com/rdkit/rdkit/discussions/4593
    z_atoms = from_smiles(r'C/C1CCCCC1\C')
    e_atoms = from_smiles(r'C/C1CCCCC1/C')

    assert to_smiles(z_atoms) != to_smiles(e_atoms)


def test_cis_trans_but2ene():
    # see https://en.wikipedia.org/wiki/But-2-ene
    cis_atoms = from_smiles(r'C/C=C\C')
    trans_atoms = from_smiles(r'C/C=C/C')

    assert to_smiles(cis_atoms) != to_smiles(trans_atoms)
