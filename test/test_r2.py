from sampling.compare import r2


def test_simple():
    observed = [-2, 2]
    predicted = [-1, 1]

    assert r2(observed, predicted) == 1 - 2 / 8
