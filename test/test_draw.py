from ase.build import molecule
from sampling.draw import draw


def test_draw():
    atoms = molecule('H2O')
    result = draw(atoms)
    # test for pillow type
    assert 'PIL' in str(type(result))
