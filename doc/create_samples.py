from ase import io
from rdkit.Chem import AllChem

from sampling.translate import atoms_to_rdkit, rdkit_to_atoms


name = 'S4'

# created by avogadro2
m = atoms_to_rdkit(io.read(name + '.cml'))
#rdkit_to_atoms(m).edit()
    
# see https://www.rdkit.org/docs/Cookbook.html#conformer-generation-with-etkdg
params = AllChem.ETKDG()
params.useSmallRingTorsions = True

cids = AllChem.EmbedMultipleConfs(m, numConfs=50, params=params)
print(len(cids))

with io.Trajectory(name + '_conformers.traj', 'w') as traj:
    for c in m.GetConformers():
        traj.write(rdkit_to_atoms(m, c.GetPositions()))
