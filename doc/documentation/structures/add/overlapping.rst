=================
Adding structures
=================

We want to replace the hydrogens in
NH\ :sub:`4`\ with C(CH\ :sub:`3`\ )\ :sub:`3`\ groups.

.. literalinclude:: add_nh4_C(CH3)3.py

The results shows overlapping methyl groups which we want to avoid.
This can be achieved by force-filed relaxing the structre during the
addition process.

.. literalinclude:: add_nh4_C(CH3)3_optimized.py
