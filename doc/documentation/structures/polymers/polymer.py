from sampling.translate import from_smiles
from sampling.construct import repeat_monomer

atoms = from_smiles(r'C/CC\C')

polymer = repeat_monomer(atoms, [4, 12], 10, optimize=True)
polymer.edit()
